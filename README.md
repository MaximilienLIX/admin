# admin
A simple bash script for an archlinux administrator.

# Installation by AUR

PKGBUILD available at this address: https://aur.archlinux.org/packages/admin-git/

You can install it in one command with yaourt.

```bash 
yaourt admin-git
```

Of course, the package use the latest source code and by consequence the program maybe unstable. A version release will come soon.
