#!/usr/bin/env bash

RESETC="$(tput sgr0)"
YELLOW="$(tput bold ; tput setaf 3)"
RED="$(tput bold ; tput setaf 1)"
GREEN="$(tput bold ; tput setaf 2)"

source /etc/admin/admin.rc

kernel_detector()
{
     kernel_lts=`uname -r | grep *-lts`
     kernel_ck=`uname -r | grep *-ck`
                if $kernel_lts ; then
                my_kernel="linux-lts"
                elif $kernel_ck; then
                my_kernel="linux-ck"
                else
                my_kernel="linux"
                fi
}

grub_detector()
{
     if [ ! -e "/usr/bin/grub-mkconfig"]; then
     exit 0
     fi
}


editor_detector()
{
     if [ ! -e "/usr/bin/$EDITOR"] ; then
     echo -e " The $EDITOR doesn't exist or not configurate !\n"
     exit 0
     fi
}

argument_detector()
{
     if [ -z $1 ] ; then
     echo -e "${RED}ERROR${RESETC}\nInvalid command !\nRun admin -help for more information !"
     exit 0
     fi
}

package-manager_admin()
{
     case $1 in
             "pkg")
             case $2 in
                     "edit")
                     $root_command $EDITOR $package_manager_edit
                     ;;
                     "update")
                     $root_command $package_manager_update
                     ;;
                     *)
                     echo -e "${RED}ERROR${RESETC}\nInvalid command !\nRun admin -help for more information !"
                     ;;
             esac
             ;;
      esac
}

kernel_admin()
{
     case $1 in
             "kernel")
             case $2 in
                     "edit")
                     $root_command $EDITOR $kernel_edit
                     ;;
                    "update")
                    kernel_detector
                    $root_command $kernel_update $my_kernel
                    ;;
                    *)
                    echo -e "${RED}ERROR${RESETC}\nInvalid command !\nRun admin -help for more information !"
                    ;;
              esac
            ;;
     esac
}

grub_admin()
{
    case $1 in
            "grub")
             case $2 in
                "edit")
                $root_command $EDITOR $grub_edit
                ;;
                "update")
                $root_command $grub_update
                ;;
                *)
                echo -e "${RED}ERROR${RESETC}\nInvalid command !\nRun admin -help for more information !"
                ;;
              esac
              ;;
     esac
}

bash_admin()
{
    case $1 in
            "bash")
            case $2 in
                "edit")
                $EDITOR $HOME/.bashrc
                ;;
                "update")
                exec bash
                ;;
                *)
                echo -e "${RED}ERROR${RESETC}\nInvalid command !\nRun admin -help for more information !"
                ;;
             esac
             ;;
     esac
}

firefox_admin()
{
     case $1 in
             "firefox")
             if [ -e  "/usr/bin/firefox" ]
             then
             case $2 in
                     "sqlite")
                     for i in ~/.mozilla/firefox/*/*.sqlite; do sqlite3 $i 'VACUUM;'; done
                     echo -e "${GREEN}Success !${RESETC}"
                     ;;
                     "profile")
                     $firefox_profile
                     ;;
                     *)
                     echo -e "${RED}ERROR${RESETC}\nInvalid command !\nRun admin -help for more information !"
                     ;;
              esac
              else
              echo -e "${RED}ERROR${RESETC} Firefox is not installed !"
              fi
          ;;
     esac
}

info_admin()
{
    case $1 in
            "info")
            cat << "EOF"

    #####################################
    #              _           _        #
    #     /\      | |         (_)       #
    #    /  \   __| |_ __ ___  _ _ __   #
    #   / /\ \ / _` | '_ ` _ \| | '_ \  #
    #  / ____ \ (_| | | | | | | | | | | #
    # /_/    \_\__,_|_| |_| |_|_|_| |_| #
    #                                   #
    #####################################

                 version 0.1

EOF
            ;;
     esac
}

help_admin()
{
     case $1 in
             "-help")
             $manual
             ;;
     esac
}

xorg_admin()
{
     case $1 in
             "xorg")
             case $2 in
                     "edit")
                     $EDITOR $session_cfg
                     ;;
                     "update")
                     $session_cfg
                     ;;
                     *)
                     echo -e "${RED}ERROR${RESETC} Firefox is not installed !"
                     ;;
              esac
              ;;
      esac
}





